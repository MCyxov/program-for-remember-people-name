import sqlite3

from PyQt5.QtWidgets import QFrame, QMessageBox, QPushButton, QVBoxLayout
from PyQt5.QtCore import pyqtSignal

from remuve_some_person import remuve_person
from test import testwindow
from add import add_window
from sleeps import sleeps_window
from add_of_file import window
from test_m import test_m


class Window(QFrame):
    statusUpdated = pyqtSignal(str)

    def __init__(self):
        super().__init__()

        self.secondWin = None

        self.init_ui()

    def init_ui(self):
        start_eas = QPushButton("Начать тестирование (просто)", self)
        start_eas.clicked.connect(self.start)

        start_m = QPushButton("Начать тестирование (средне)", self)
        start_m.clicked.connect(self.start_m)

        add = QPushButton("Добавить профиль", self)
        add.clicked.connect(self.add)

        add_of_file = QPushButton("Добавить профиль из генератоора", self)
        add_of_file.clicked.connect(self.add_of_file)

        remove = QPushButton("Удалить всех", self)
        remove.clicked.connect(self.remove)

        sleeps = QPushButton("Спать", self)
        sleeps.clicked.connect(self.sleeps)

        rem = QPushButton("Удалить одного человека", self)
        rem.clicked.connect(self.rem)

        horizontal = QVBoxLayout()
        horizontal.addWidget(start_eas)
        horizontal.addWidget(start_m)
        horizontal.addWidget(add_of_file)
        horizontal.addWidget(add)
        horizontal.addWidget(rem)
        horizontal.addWidget(remove)
        horizontal.addWidget(sleeps)
        self.setLayout(horizontal)

        self.setGeometry(400, 400, 400, 250)
        self.setWindowTitle('menu')
        self.show()

    def start(self):
        conn = sqlite3.connect("people.db")
        cursor = conn.cursor()
        try:
            cursor.execute("CREATE TABLE people(path text, name text, name2 text)")
        except sqlite3.OperationalError:
            pass
        cursor.execute("SELECT * from people")
        data = cursor.fetchall()
        i = len(data)
        conn.close()
        if i < 6:
            QMessageBox.about(self, 'Привет!',
                              'Что бы начать пользоваться программой надо занести в базу хотя бы пять человек')
            return
        self.hide()
        self.secondWin = testwindow.TestWindow()

    def add(self):
        self.hide()
        self.secondWin = add_window.Window()

    def remove(self):
        button_reply = QMessageBox.question(self, 'Mind test', "Ты уверен?",
                                            QMessageBox.Yes | QMessageBox.No)
        if button_reply == QMessageBox.No:
            return
        conn = sqlite3.connect("people.db")
        cursor = conn.cursor()
        cursor.execute("DROP TABLE IF EXISTS people")
        conn.commit()
        conn.close()

    def sleeps(self):
        self.hide()
        self.secondWin = sleeps_window.Window()

    def rem(self):
        self.hide()
        self.secondWin = remuve_person.Window()

    def add_of_file(self):
        window.Add()
        QMessageBox.about(self, 'Minde test',
                          'Загружено')

    def start_m(self):
        self.hide()
        self.secondWin = test_m.TestWindow()
