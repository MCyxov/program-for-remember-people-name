from PyQt5.QtWidgets import QFrame, QMessageBox, QPushButton, QVBoxLayout, QTextEdit
from PyQt5.QtCore import pyqtSignal

from time import sleep

from menu import window


class Window(QFrame):
    statusUpdated = pyqtSignal(str)

    def __init__(self):
        super().__init__()

        self.second_window = None
        self.name = None

        self.init_ui()

    def init_ui(self):
        seep = QPushButton("Заснуть", self)
        seep.clicked.connect(self.sleeps)

        self.name = QTextEdit("Время в минутах", self)

        back = QPushButton("назад", self)
        back.clicked.connect(self.back)

        horizontal = QVBoxLayout()
        horizontal.addWidget(self.name)
        horizontal.addWidget(seep)
        horizontal.addWidget(back)
        self.setLayout(horizontal)

        self.setLayout(horizontal)
        self.setGeometry(300, 300, 300, 150)
        self.setWindowTitle('menu')
        self.show()

    def sleeps(self):
        button_reply = QMessageBox.question(self, 'Mind test', "Ты уверен?",
                                            QMessageBox.Yes | QMessageBox.No)
        if button_reply == QMessageBox.No:
            return
        self.hide()
        try:
            sleep(float(self.name.toPlainText()) * 60)
        except TypeError:
            QMessageBox.about(self, 'Ты не прав', 'Нужно число')
        self.second_window = window.Window()

    def back(self):
        self.hide()
        self.second_window = window.Window()
