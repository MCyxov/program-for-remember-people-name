#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import QApplication

from menu import window

if __name__ == '__main__':
    app = QApplication([])
    menu = window.Window()
    sys.exit(app.exec_())
