import sqlite3

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QFrame, QMessageBox, QPushButton, QVBoxLayout, QTextEdit

from menu import window


class Window(QFrame):
    statusUpdated = pyqtSignal(str)

    def __init__(self):
        super().__init__()

        self.secondWin = None

        self.initUI()

    def initUI(self):
        remuve = QPushButton("Удалить", self)
        remuve.clicked.connect(self.rem)

        see = QPushButton("Посмотреть кто есть", self)
        see.clicked.connect(self.see)

        self.name = QTextEdit("Имя и фамилия(как записанно в базе) человека, которого вы хотите удалить", self)

        back = QPushButton("назад", self)
        back.clicked.connect(self.back)

        gorizontal = QVBoxLayout()
        gorizontal.addWidget(self.name)
        gorizontal.addWidget(see)
        gorizontal.addWidget(remuve)
        gorizontal.addWidget(back)
        self.setLayout(gorizontal)

        self.setLayout(gorizontal)
        self.setGeometry(300, 300, 300, 150)
        self.setWindowTitle('menu')
        self.show()

    def rem(self):
        buttonReply = QMessageBox.question(self, 'Mind test', "Ты уверен?",
                                           QMessageBox.Yes | QMessageBox.No)
        if buttonReply == QMessageBox.No:
            return
        conn = sqlite3.connect("people.db")
        cursor = conn.cursor()
        s1, s2 = self.name.toPlainText().strip().split(' ')
        cursor.execute("DELETE FROM people WHERE name = '%s' AND name2 = '%s'" % (s1, s2))
        conn.commit()
        conn.close()

    def see(self):
        conn = sqlite3.connect("people.db")
        cursor = conn.cursor()
        try:
            cursor.execute("SELECT * from people")
        except sqlite3.OperationalError:
            QMessageBox.about(self, "error", "Database does not exist")
            return
        data = cursor.fetchall()
        conn.close()
        s = ''
        for i in range(len(data)):
            s += data[i][1] + ' ' + data[i][2] + '\n'
        QMessageBox.about(self, 'Все, все, все', s)

    def back(self):
        self.hide()
        self.second_window = window.Window()

