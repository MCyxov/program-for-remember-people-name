import sqlite3

from PIL import Image
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QFrame, QMessageBox, QPushButton, QTextEdit, QFileDialog, QVBoxLayout

from menu import window


class Window(QFrame):
    statusUpdated = pyqtSignal(str)

    def __init__(self):
        super().__init__()

        self.secondWin = None
        self.name = None
        self.name2 = None
        self.second_window = None
        self.filename = ''

        self.init_ui()

    def init_ui(self):
        add = QPushButton("Сохранить", self)
        add.clicked.connect(self.add)

        file = QPushButton("Выбрать файл", self)
        file.clicked.connect(self.open_file_name_dialog)

        back = QPushButton("назад", self)
        back.clicked.connect(self.back)

        self.name = QTextEdit("имя", self)

        self.name2 = QTextEdit("фамилия", self)

        vertical = QVBoxLayout()
        vertical.addWidget(file)
        vertical.addWidget(self.name)
        vertical.addWidget(self.name2)
        vertical.addWidget(back)
        vertical.addWidget(add)

        self.setLayout(vertical)
        self.setGeometry(300, 300, 300, 150)
        self.setWindowTitle('menu')
        self.show()

    def add(self):
        size = 360, 360
        path = self.filename
        if self.filename is None:
            QMessageBox.about(self, 'Ты не прав', 'Выбери файл')
            return
        im = Image.open(path)
        im.thumbnail(size, Image.ANTIALIAS)
        conn = sqlite3.connect("people.db")
        cursor = conn.cursor()
        try:
            cursor.execute("CREATE TABLE people(path text, name text, name2 text)")
        except Exception:
            pass
        cursor.execute("SELECT * from people")
        data = cursor.fetchall()
        i = int(data[len(data) - 1][0][3]) + 1
        im.save('./foto/alk%s.jpg' % i, "JPEG")
        cursor.execute("INSERT INTO people VALUES(?, ?, ?)",
                       ('alk%i.jpg' % i, self.name.toPlainText(), self.name2.toPlainText()))
        conn.commit()
        conn.close()
        self.hide()
        self.second_window = window.Window()
        self.filename = None

    def open_file_name_dialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_name, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                   "All Files (*);;Python Files (*.py)", options=options)
        if file_name:
            self.filename = file_name

    def back(self):
        self.hide()
        self.second_window = window.Window()
